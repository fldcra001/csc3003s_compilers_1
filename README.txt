##############################
CSC3003S Compilers
Assignment 1: Lexical and Syntactic Analysis
README.txt

Craig Feldman

10 September 2014
#############################

------------
Instructions
------------
Extract the contents of the archive to disk. 

Run the command 'java -jar lib/sablecc.jar grammar.txt' to generate SableCC files.
Run the command 'javac Lex.java Parse.java Printer.java' to generate class files. 

To run the lexical analysis, use the command "java Lex [file.nul]".
To generate the parse tree, use the command "java Parse [file.nul]".
The output will be displayed to console and written to disk (in the lexical_outputs or parse_outputs folder)

-----
Note
-----
This application was developed using JavaSE-1.7 and SableCC v3.7 under Windows 8.
The application has been tested on Linux.

When printing the parsetree, nodes may be named slightly differently to the test output files.

I have included the test files in two folders, 'lexical_samples' and 'parse_samples'.
To run the application using these files, you need to provide the file path, eg. java Lex lexical_samples/floats.nul

Outputs are written to console and disk.
Parse outputs are written to the folder parse_outputs.
Lexical outputs are written to the folder lexical_outputs.

The grammar file is included ('grammar.txt'). 
All SableCC files are generated using the command 'java -jar lib/sablecc.jar grammar.txt'.

For your conveniance, I have included the commands to test the program using the supplied test files:
--------------------------------------------------
Lexical Tests
java Lex lexical_samples/program.nul

[
java Lex lexical_samples/comments.nul
java Lex lexical_samples/expression.nul
java Lex lexical_samples/floats.nul
java Lex lexical_samples/identifiers.nul
java Lex lexical_samples/keywords.nul
java Lex lexical_samples/operators.nul
java Lex lexical_samples/whitespace.nul
]

Parse Tests
java Parse parse_samples/program.nul

[
java Parse parse_samples/assignment.nul
java Parse parse_samples/expression.nul
java Parse parse_samples/function.nul
java Parse parse_samples/statements.nul
]
--------------------------------------------------

Enjoy!



--------------------------------------------------
ASSIGNMENT INSTRUCTIONS
--------------------------------------------------

Introduction

In this assignment you will create a program which does lexical analysis and a program which does syntactic analysis for a made-up programming language called nulang, for number language.

The lexical analyser (lexer) program should check a specified *.nul nulang input program file and convert it into the correct tokens, outputting the tokens to the screen and also into a corresponding *.tkn file.

The syntactic analyser (parser) program should check a specified *.nul nulang input program file conforms to the specified grammar and generate an appropriate syntax (parse) tree which is output to the screen and to a corresponding *.str file.

Tools

The programming language and compiler tools for this assignment can be either

Java with SableCC, JavaCC, or JFlex and Cup
Python with PLY
C++ with Flex and Bison
Although it is strongly recommended that you use Java with SableCC, since that is what the reference solution is implemented in and will allow for easy integration with Compilers Assignment 2.

Input, Output and Testing

The input *.nul source code file should be specified as a command line parameter when your programs are run, e.g.

                lex  my_program.nul

                parse  my_program.nul

The lexical analyser (lexer) should check the file for tokens based on the definitions below and print each token on a new line to the screen and also in a corresponding token file, my_program.tkn. The details of how each token should be printed are specified below.

The syntactic analyser (parser) should check the tokens conform to the grammar defined below and construct and output the syntax (parse) tree as flat text using depth-first traversal, visiting the root, then children from left to right onto the screen and also in a corresponding file, my_program.str.

Download the lex_nulang_samples.zip file containing *.nul code input files and their corresponding output *.tkn files, and the parse_nulang_samples.zip file containing *.nul code input files and their corresponding output *.str files, indicating what the output should look like and can be used to test your program.

Note: The syntax tree your program generates may differ slightly from the samples provided, with possibly slightly more or less internal nodes, but the tokens should be in the correct place relative to each other. 

Tokens

Keywords

There is only one keyword string:    func

Output: The capitalized keyword.

Example: If the string 'func' is encountered 'FUNC' should be the token that is output.

Identifiers

An identifier is a sequence of letters digits and underscores, starting with either a letter or an underscore. Identifiers are also case sensitive.

Output: ID,_VALUE_

Example: If the string 'myNum' is encountered 'ID,myNum' should be the token that is output.

Numeric Literals

All numeric literals should be recognised as floats, which will make calculations simpler later on. Numeric literals consist of an integer part, an optional decimal part, and an optional exponent suffix part respectively. The integer part is a sequence of one or more digits. The decimal part is a decimal point followed by 0, 1 or more digits. The exponent part is the character e or E followed by an optional + or - sign, followed by one or more digits. The decimal and the exponent part are both optional.

Output: FLOAT_LITERAL,_VALUE_

Example: If the string '12.34' is encountered 'FLOAT_LITERAL,12.34' should be the token that is output.

Operators

An operator can be one of the following operators:    +   -   *   /   =   (   )   {   }

                Hint: It may be useful to define a separate rule for each operator.

Output: Each operator should be its own token.

Example: If the string '(' is encountered '(' should be the token that is output.

Whitespace

Whitespace is sequence of non-printable characters. Non-printable characters includes:

space (' '), tab ('\t'), newline ('\n'), carriage return ('\r')...

Output: WHITESPACE

Example: If the tab character '\t' is encountered ' WHITESPACE ' should be the token that is output.

Comments

There are two forms of comments: One starts with /*, ends with */; another begins with // and goes to the end of the line (the end of line character should not be included in this token, rather it should be handled by the whitespace token.)

Output: COMMENT

Example: If the string '// a comment' is encountered ' COMMENT ' should be the token to be output.

Grammar

The nulang grammar uses the notation N∗, for a non-terminal N, meaning 0, 1, or more repetitions of N. Bold symbols are keywords and should form their own tokens, and other tokens are in italics.

Program               → FunctionDecl* Statement*                                                       // these asterisks indicate closure

FunctionDecl      → FunctionHead FunctionBody

FunctionHead    → func identifier ( identifier )

FunctionBody    → { Statement* }                                                                            // this asterisk indicates closure

Statement          → identifier = Expression

Expression          → Expression + Term

                                → Expression - Term

                                → Term

Term                    → Term * Factor                                                                       // this asterisk indicates multiplication

                                → Term / Factor

                                → Factor

Factor                  → ( Expression )

                                → identifier ( Expression )

                                → float

                                → identifier

Due