/* This file was generated by SableCC (http://www.sablecc.org/). */

package nulang.node;

import nulang.analysis.*;

@SuppressWarnings("nls")
public final class TClosebracket extends Token
{
    public TClosebracket()
    {
        super.setText(")");
    }

    public TClosebracket(int line, int pos)
    {
        super.setText(")");
        setLine(line);
        setPos(pos);
    }

    @Override
    public Object clone()
    {
      return new TClosebracket(getLine(), getPos());
    }

    @Override
    public void apply(Switch sw)
    {
        ((Analysis) sw).caseTClosebracket(this);
    }

    @Override
    public void setText(@SuppressWarnings("unused") String text)
    {
        throw new RuntimeException("Cannot change TClosebracket text.");
    }
}
