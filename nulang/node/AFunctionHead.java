/* This file was generated by SableCC (http://www.sablecc.org/). */

package nulang.node;

import nulang.analysis.*;

@SuppressWarnings("nls")
public final class AFunctionHead extends PFunctionHead
{
    private TFunc _func_;
    private TIdentifier _name_;
    private TOpenbracket _openbracket_;
    private TIdentifier _parameter_;
    private TClosebracket _closebracket_;

    public AFunctionHead()
    {
        // Constructor
    }

    public AFunctionHead(
        @SuppressWarnings("hiding") TFunc _func_,
        @SuppressWarnings("hiding") TIdentifier _name_,
        @SuppressWarnings("hiding") TOpenbracket _openbracket_,
        @SuppressWarnings("hiding") TIdentifier _parameter_,
        @SuppressWarnings("hiding") TClosebracket _closebracket_)
    {
        // Constructor
        setFunc(_func_);

        setName(_name_);

        setOpenbracket(_openbracket_);

        setParameter(_parameter_);

        setClosebracket(_closebracket_);

    }

    @Override
    public Object clone()
    {
        return new AFunctionHead(
            cloneNode(this._func_),
            cloneNode(this._name_),
            cloneNode(this._openbracket_),
            cloneNode(this._parameter_),
            cloneNode(this._closebracket_));
    }

    @Override
    public void apply(Switch sw)
    {
        ((Analysis) sw).caseAFunctionHead(this);
    }

    public TFunc getFunc()
    {
        return this._func_;
    }

    public void setFunc(TFunc node)
    {
        if(this._func_ != null)
        {
            this._func_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._func_ = node;
    }

    public TIdentifier getName()
    {
        return this._name_;
    }

    public void setName(TIdentifier node)
    {
        if(this._name_ != null)
        {
            this._name_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._name_ = node;
    }

    public TOpenbracket getOpenbracket()
    {
        return this._openbracket_;
    }

    public void setOpenbracket(TOpenbracket node)
    {
        if(this._openbracket_ != null)
        {
            this._openbracket_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._openbracket_ = node;
    }

    public TIdentifier getParameter()
    {
        return this._parameter_;
    }

    public void setParameter(TIdentifier node)
    {
        if(this._parameter_ != null)
        {
            this._parameter_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._parameter_ = node;
    }

    public TClosebracket getClosebracket()
    {
        return this._closebracket_;
    }

    public void setClosebracket(TClosebracket node)
    {
        if(this._closebracket_ != null)
        {
            this._closebracket_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._closebracket_ = node;
    }

    @Override
    public String toString()
    {
        return ""
            + toString(this._func_)
            + toString(this._name_)
            + toString(this._openbracket_)
            + toString(this._parameter_)
            + toString(this._closebracket_);
    }

    @Override
    void removeChild(@SuppressWarnings("unused") Node child)
    {
        // Remove child
        if(this._func_ == child)
        {
            this._func_ = null;
            return;
        }

        if(this._name_ == child)
        {
            this._name_ = null;
            return;
        }

        if(this._openbracket_ == child)
        {
            this._openbracket_ = null;
            return;
        }

        if(this._parameter_ == child)
        {
            this._parameter_ = null;
            return;
        }

        if(this._closebracket_ == child)
        {
            this._closebracket_ = null;
            return;
        }

        throw new RuntimeException("Not a child.");
    }

    @Override
    void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild)
    {
        // Replace child
        if(this._func_ == oldChild)
        {
            setFunc((TFunc) newChild);
            return;
        }

        if(this._name_ == oldChild)
        {
            setName((TIdentifier) newChild);
            return;
        }

        if(this._openbracket_ == oldChild)
        {
            setOpenbracket((TOpenbracket) newChild);
            return;
        }

        if(this._parameter_ == oldChild)
        {
            setParameter((TIdentifier) newChild);
            return;
        }

        if(this._closebracket_ == oldChild)
        {
            setClosebracket((TClosebracket) newChild);
            return;
        }

        throw new RuntimeException("Not a child.");
    }
}
