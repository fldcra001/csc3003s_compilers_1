/* This file was generated by SableCC (http://www.sablecc.org/). */

package nulang.node;

import nulang.analysis.*;

@SuppressWarnings("nls")
public final class TOpenbracket extends Token
{
    public TOpenbracket()
    {
        super.setText("(");
    }

    public TOpenbracket(int line, int pos)
    {
        super.setText("(");
        setLine(line);
        setPos(pos);
    }

    @Override
    public Object clone()
    {
      return new TOpenbracket(getLine(), getPos());
    }

    @Override
    public void apply(Switch sw)
    {
        ((Analysis) sw).caseTOpenbracket(this);
    }

    @Override
    public void setText(@SuppressWarnings("unused") String text)
    {
        throw new RuntimeException("Cannot change TOpenbracket text.");
    }
}
