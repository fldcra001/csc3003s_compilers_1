/*
 * Performs lexical analysis on a file provided at execution
 * Outputs the tokens to console and disk
 * 
 * Craig Feldman
 * 8 September 2014
 */

//package nulang;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.PushbackReader;

import nulang.lexer.Lexer;
import nulang.node.EOF;
import nulang.node.TComment;
import nulang.node.TFloat;
import nulang.node.TFunc;
import nulang.node.TIdentifier;
import nulang.node.TWhitespace;
import nulang.node.Token;


public class Lex {

	public static void main(String[] args) throws IOException {
		
		if (args.length != 1) {
			System.out.println("Incorrect number of arguments.");
			System.out.println("Proper usage is 'java Lex file.nul'");
			System.exit(0);
		}
		

		//input and output file locations
		String inputFile = args[0];		
		String outputFile = "lexical_outputs" + inputFile.substring(inputFile.indexOf("/"), inputFile.indexOf(".")) + ".tkn";
		
		FileReader inFile = null;
		PrintWriter outFile = null;
		
		System.out.println("\nGenerating tokens for file '" + inputFile +"'");
		System.out.println("Outputing to console and '" + outputFile + "'");
		System.out.println();
		
			
        try {
            inFile = new FileReader(inputFile);
    		outFile = new PrintWriter(outputFile);
            Lexer lexer = new Lexer(new PushbackReader(new BufferedReader(inFile),1024));
            
            String toOutput = null;
    		System.out.println("--------------BEGIN--------------\n");
            // loop through all the tokens
            Token next = null;
            while (!(next instanceof EOF)) {
            	next = lexer.next();
            	
            	if (next instanceof TComment)
            		toOutput = "COMMENT";
            	
            	else if (next instanceof TFunc)
            		toOutput = "FUNC";
            	
            	else if (next instanceof TWhitespace)
            		toOutput = "WHITESPACE";
            	
            	else if (next instanceof TIdentifier)
            		toOutput = "ID," + next;
            	
            	else if (next instanceof TFloat)
            		toOutput = "FLOAT_LITERAL," + next;

            	else // operator
            		toOutput = next.toString();
            	
            	System.out.println(toOutput);
            	outFile.println(toOutput);
            }
    		System.out.println("---------------END---------------");

        
        } catch (FileNotFoundException e) {
        	System.out.println("File not found!");
        } catch (Exception e) {
            throw new RuntimeException("\n"+e.getMessage());
        } finally {
        	inFile.close();
        	outFile.close();        	
        }
        
    }
}
	


