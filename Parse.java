/*
 * Parses the a parse tree according to the specified grammar (grammar.txt)
 * 
 * Craig Feldman
 * 9 September 2014
 */

//package nulang;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.PushbackReader;

import nulang.lexer.Lexer;
import nulang.node.Start;
import nulang.node.Token;
import nulang.parser.Parser;

public class Parse {

	public static void main(String[] args) throws IOException {
		
		if (args.length != 1) {
			System.out.println("Incorrect number of arguments.");
			System.out.println("Proper usage is 'java Parse file.nul'");
			System.exit(0);
		}
		
		String inputFile = args[0];
		String outputFile = "parse_outputs" + inputFile.substring(inputFile.indexOf("/"), inputFile.indexOf(".")) + ".str";
		
		FileReader inFile = null;
		PrintWriter outFile = null;
		
		System.out.println("\nGenerating parse tree for file '" + inputFile +"'");
		System.out.println("Outputting to console and '" + outputFile +"'");
		
		 try {
	            inFile = new FileReader(inputFile);
	    		outFile = new PrintWriter(outputFile);
	    		
	            Lexer lexer = new Lexer(new PushbackReader(new BufferedReader(inFile),1024));    
	            Parser p = new Parser(lexer);
	           
	            Start tree = p.parse();
	            Printer P = new Printer();	            
	            tree.apply(P);
	            String toOutput = P.getOutput();
	            
	            //write output to console and file
	    		System.out.println("\n--------------BEGIN TREE--------------");
	            System.out.println(toOutput);
	    		System.out.println("--------------END TREE--------------");

	            outFile.write(toOutput);
	            
	        } catch (FileNotFoundException e) {
	        	System.out.println("File not found!");
	        } catch (Exception e) {
	        	System.out.println("Runtime exception!");
	            throw new RuntimeException("\n"+e.getMessage());
	        } finally {
	        	inFile.close();
	        	outFile.close();        	
	        }
	        
	}

}
