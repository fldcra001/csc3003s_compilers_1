/*
 * Performs depth first transversal on parse tree
 * Outputs the parse tree to console and disk
 *
 * Craig Feldman
 * 08 September 2014
 */

//package nulang;
import nulang.node.*;
import nulang.analysis.*;

public class Printer extends DepthFirstAdapter {
	
	// how much we need to indent when printing
	int indent;
	
	//the string that will be outputted to console
	String toOutput = "";
	
	//returns a string containing the indent
	private String getIndent() {
		String s ="";
		for (int i = 0; i < indent; ++i)
			s += "\t";
		return s;
	}
	
	// builds string to output
	private void printNode(Node node)
	{
		// add the indent to the output string that will be built on
		toOutput += "\n" + getIndent();
		
		//adds the type of node
		toOutput += ((node.getClass().toString()).replace("class nulang.node.", ""));
	}
	
	
	// builds onto string to output
	@Override
	public void defaultCase (Node node) {
		toOutput += "\n" + getIndent();
		if (node instanceof TIdentifier)
			toOutput += "ID," + node;
		
		else if (node instanceof TFloat)
			toOutput += "FLOAT_LITERAL," + node;
		
		else if (node instanceof TFunc)
			toOutput +=  "FUNC";
		
		else
			toOutput += node;
	}
	
	@Override
	public void defaultIn(Node node) {
		printNode(node);
		indent++;
	}

	@Override
	public void defaultOut(Node node) {
		indent--;
	}
	
	public String getOutput() {
		return toOutput;
	}
	
}
